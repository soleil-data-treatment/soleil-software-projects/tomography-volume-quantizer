#/bin/bash

#  Usage: ./tvq_copy.sh [-8|-s|-h] INPATH OUTPATH > myscript.sh
#    Outputs a script file (myscript.sh) to be executed afterwards
#    The script will quantize all .vol files (change bit-level) found in 
#    INPATH and copy them in OUTPATH while keeping the tree structure.
#  Options:
#   -s   Format the generated script fas a SLURM job.
#   -8   Quantize the initial volume to 8 bits (default is 16).
#   -h   This help.


# set -x

dir_exists() {
    local folder=$1
    if [ ! -d "${folder}" ]; then
        echo "$0: Directory ${folder} does not exist"
        exit 1
    fi
}

usage() {
    echo "Usage: $0 [-8|-s|-h] INPATH OUTPATH > myscript.sh"
    echo "  Outputs a script file (myscript.sh) to be executed afterwards"
    echo "  The script will quantize all .vol files (change bit-level) found in "
    echo "  INPATH and copy them in OUTPATH while keeping the tree structure."
    echo "Options:"
    echo " -s   Format the generated script fas a SLURM job."
    echo " -8   Quantize the initial volume to 8 bits (default is 16)."
    echo " -h   This help."
}

# scan input options
# Get the options
SLURM=0
QUANTIZATION="main16.exe"
while getopts ":hs8" option; do
   case $option in
      h) # display Help
         usage
         exit;;
      s) # SLURM
         SLURM=1;;
      8) # Quantization level (8)
         QUANTIZATION="main8.exe"
         ;;
   esac
done

shift $((OPTIND-1))

if [ $# -lt 2 ]; then
    usage
    exit 1
fi

INPATH=${1%/}  # remove trailing "/" character in path (e.g., "/tmp/toto/" -> "/tmp/toto")
OUTPATH=${2%/} # idem

dir_exists ${INPATH}
dir_exists ${OUTPATH}

if [ $SLURM = 1 ]; then
    # we work in 'cluster' mode
    echo "#!/bin/bash"

    echo "#SBATCH --output=output_%A_%a.log"
    echo "#SBATCH --error=error_%A_%a.log"
    echo "#SBATCH --partition=main"
    echo "#SBATCH --job-name=convert_$INPATH"
    echo "#SBATCH --time=1800:00"
    echo "#SBATCH --ntasks=1"
    echo "#SBATCH --cpus-per-task=1"
    echo "#SBATCH --mem-per-cpu=MEMSIZE"
    echo " "
fi

# In the loop below, we define INFILE = INPATH / SUBDIR / FILENAME
for INFILE in $(find ${INPATH} -name "*.vol" -type f); do
    INDIR=${INFILE%/*}
    # Deal with the case when SUBDIR is empty
    if [ "${INDIR}" = "${INPATH}" ]; then
        SUBDIR=
        OUTDIR="${OUTPATH}"
    else
        SUBDIR=${INDIR#$INPATH/}
        OUTDIR="${OUTPATH}/${SUBDIR}"
        mkdir -p ${OUTDIR}
    fi
    FILENAME=${INFILE##*/}
    OUTFILE="${OUTDIR}/${FILENAME%.vol}_16b.vol"
    
    # check if the file has not been processed or is empty
    PROCESS=0;
    if [ -s "${OUTFILE}" ]; then
      PROCESS=1;
    fi
    if [ ! -f "${OUTFILE}" ]; then
      PROCESS=1;
    fi
    if [ $PROCESS = 1 ]; then
        echo "####"
        # Retrieve some values from .vol.info file and launch main script
        INFILE_INFO="${INFILE}.info"
        if [ ! -f "${INFILE_INFO}" ]; then
            >&2 echo "Could not find the .info file: ${INFILE_INFO}"
        else
            NX=$(cat ${INFILE_INFO} | grep "NUM_X" | cut -d "=" -f 2 | tr -d ' ')
            NY=$(cat ${INFILE_INFO} | grep "NUM_Y" | cut -d "=" -f 2 | tr -d ' ')
            CHUNK_LEN=$(echo "${NX} * ${NY}" | bc)
            VALUE_LOW=$(cat ${INFILE_INFO} | grep "S1" | cut -d "=" -f 2 | tr -d ' ')
            VALUE_HIGH=$(cat ${INFILE_INFO} | grep "S2" | cut -d "=" -f 2 | tr -d ' ')

            

            echo "INFILE=${INFILE}"
            echo "OUTFILE=${OUTFILE}"
            echo  "./$QUANTIZATION" '${INFILE}' "-c ${CHUNK_LEN} -a ${VALUE_LOW} -b ${VALUE_HIGH}" '-o ${OUTFILE}'
        fi
    else
        >&2 echo "OUTFILE ${OUTFILE} already exists. Nothing to do."
    fi
 done

