# Tomography Volume Quantizer

Scripts to quantize a 32-bit floating-point little-endian binary file (.vol) generated by tomography experiment on 8- or 16-bit integers.


# Compilation
```
./create_exe.sh
```
For 8-bit quantization:
```
ln -s main8.exe main.exe
```
For 16-bit quantization:
```
ln -s main16.exe main.exe
```

# Usage
```
./main.exe -h
```

Basic usage using the input volume file `INFILE`, saving the quantized volume in the file `OUTFILE`:
```
./main.exe -o OUTFILE INFILE

```
It is possible to specify the pixel value limits `MIN` and `MAX` (default values are defined in `src/config.h`):
```
./main.exe -o OUTFILE -a MIN -b MAX INFILE

```

Unquantization of a quantized volume `INFILE` is possible using the `-u` flag:
```
./main.exe -u -o OUTFILE INFILE
```

# Testing
```
make check
```
All volume files (raw, quantized, unquantized) are located in the directory `example`

A slice can be visualized with ImageJ or using the scripts as described below:
```
python3 scripts/read_vol.py example/raw.vol
python3 scripts/read_vol.py example/quantized.vol -d B  # for  8-bit volume
python3 scripts/read_vol.py example/quantized.vol -d H  # for 16-bit volume
python3 scripts/read_vol.py example/unquantized.vol
```

Residuals between two slices (with the same dimension and data type) can be visualized using ImageJ or as follows:
```
python3 scripts/imdiff.py example/raw.vol example/unquantized.vol
```
