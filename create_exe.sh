#/bin/bash

# For 16 bits quantization
make clean
make
mv main.exe main16.exe

# For 8 bits quantization
make clean
make CONFIG=config8.mk
mv main.exe main8.exe

ln -s main16.exe main.exe

