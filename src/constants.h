#pragma once

#define KB  1024.
#define MB  (1024*KB)
#define GB  (1024*MB)
#define MIB_TO_BYTES 1048576

#define BYTE_TO_BIT 8
