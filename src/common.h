#pragma once

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef FLAG_OPENMP
#include <omp.h>
#endif

#ifndef MIN
#define MIN(x,y) ((x)<(y)?(x):(y))
#endif
#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif

#define COUNT(x) (sizeof(x)/sizeof(x[0]))

#define STRINGIFY(X) #X
#define STRGY(X) STRINGIFY(X)

#ifdef DEBUG
#define TRACE_ON 1
#else
#define TRACE_ON 0
#endif

#define debug_print(F, ...)                                                                             \
    do {                                                                                                \
        if (TRACE_ON)                                                                                   \
            fprintf(stderr, "DEBUG -- %s, %s:" STRGY(__LINE__) ": " F, __FILE__, __func__, __VA_ARGS__); \
    } while (false)

// Use alias for input data type
typedef float custom_big_dtype_t;
#define BIG_DTYPE_FMT_STR "%f"

// Define alias for quantized data type
#ifdef FLAG_8BITS
typedef uint8_t custom_little_dtype_t;
#define INT_SIZE_MIN 0
#define INT_SIZE_MAX 255
#else
typedef uint16_t custom_little_dtype_t;
#define INT_SIZE_MIN 0
#define INT_SIZE_MAX 65535
#endif
#define INT_SIZE_RANGE (INT_SIZE_MAX - INT_SIZE_MIN)

void* safe_calloc(size_t, size_t);
FILE* safe_fopen(const char*, const char*);