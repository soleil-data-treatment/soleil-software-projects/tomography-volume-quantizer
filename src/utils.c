#include "utils.h"

bool const utils_file_exists(char const* filename) {
    bool found = false;
    if (access(filename, F_OK) == 0) {
        found = true;
    }
    return found;
}

// off_t is the same as long int 
// off_t can be negative for offsets, that's why we have an int and not an unsigned int
off_t const utils_get_file_size(char const* filename) {
    struct stat file_status;
    if (stat(filename, &file_status) < 0) {
        exit(EXIT_FAILURE);
    }
    debug_print("Input file size (bytes) = %zu\n", file_status.st_size);
    return file_status.st_size;
}

uint64_t const utils_get_file_size2(FILE* fp) {
    fseek(fp, 0L, SEEK_END);
    uint64_t const file_size = ftell(fp);
    rewind(fp);
    debug_print("Input file size (bytes) = %zu\n", file_size);
    return file_size;
}

size_t utils_get_nb_chunks(size_t const file_size, size_t const chunk_size) {
    assert(chunk_size != 0);
    assert(chunk_size <= file_size);
    assert((file_size % chunk_size) == 0);

    size_t counts = file_size / chunk_size;
    debug_print("SIZE_MAX = %zu\n", SIZE_MAX);
    debug_print("Number of chunks to process = %zu\n", counts);
    return counts;
}
