#include "common.h"

void* safe_calloc(size_t n, size_t size) {
    if (!size || !n)
        return NULL;

    if (n > SIZE_MAX / size) {
        perror("Calloc failed due to too large nmemb value.");
        errno = 0;
        exit(EXIT_FAILURE);
    }

    void *ret = calloc(n, size);

    if (!ret) {
        perror("Calloc failed.");
        errno = 0;
        exit(EXIT_FAILURE);
    }

    return ret;
}

FILE* safe_fopen(const char* filename, const char* mode) {
   FILE* fp = fopen(filename, mode);
    if (!fp) {
        perror("File cannot be open.");
        errno = 0;
        exit(EXIT_FAILURE);
    }
    return fp;
}
