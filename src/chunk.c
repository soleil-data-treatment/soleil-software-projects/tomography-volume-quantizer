#include "chunk.h"

void chunk_quantize(size_t len, void const* chunk, custom_big_dtype_t const value_low, custom_big_dtype_t const value_high, void* quantized_chunk) {
    custom_big_dtype_t const* raw_array = chunk;
    custom_little_dtype_t* quantized_array = quantized_chunk;

    custom_big_dtype_t const range = value_high - value_low;
    custom_big_dtype_t const lambda_inv = range / INT_SIZE_RANGE;

    size_t i;
    #ifdef FLAG_OPENMP
    omp_set_num_threads(omp_get_num_procs());
    #pragma omp parallel for private(i)
    #endif
    for (i = 0; i < len; ++i) {
        custom_big_dtype_t value = raw_array[i];
        if (value < value_low) {
            value = value_low;
        }
        if (value > value_high) {
            value = value_high;
        }
        custom_little_dtype_t quantized_value = round((value - value_low) / lambda_inv + INT_SIZE_MIN);
        quantized_array[i] = quantized_value;
    }
}

void chunk_unquantize(size_t len, void const* quantized_chunk, custom_big_dtype_t const value_low, custom_big_dtype_t const value_high, void* unquantized_chunk) {
    custom_little_dtype_t const* quantized_array = quantized_chunk;
    custom_big_dtype_t* unquantized_array = unquantized_chunk;

    custom_big_dtype_t const range = value_high - value_low;
    custom_big_dtype_t const lambda_inv = range / INT_SIZE_RANGE;

    size_t i;
    #ifdef FLAG_OPENMP
    omp_set_num_threads(omp_get_num_procs());
    #pragma omp parallel for private(i)
    #endif
    for (i = 0; i < len; ++i) {
        custom_little_dtype_t quantized_value = quantized_array[i];
        unquantized_array[i] = (quantized_value - INT_SIZE_MIN) * lambda_inv + value_low;
    }
}
