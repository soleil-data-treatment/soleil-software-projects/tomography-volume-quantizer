#include "chunk.h"
#include "quantizer.h"

int main(int argc, char* argv[argc+1]) {

    option o = option_get(argc, argv);

    int ret;
    if (o.unquantize) {
        memb_size_s const item_size = {
            .in = sizeof(custom_little_dtype_t),
            .out = sizeof(custom_big_dtype_t)
        };
        ret = quantizer_run(item_size, o, chunk_unquantize);
    } else {
        memb_size_s const item_size = {
            .in = sizeof(custom_big_dtype_t),
            .out = sizeof(custom_little_dtype_t)
        };
        ret = quantizer_run(item_size, o, chunk_quantize);
    }

    return ret;
}
