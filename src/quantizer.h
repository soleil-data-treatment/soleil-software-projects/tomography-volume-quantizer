#pragma once

#include "common.h"
#include "config.h"
#include "constants.h"
#include "option.h"
#include "utils.h"

typedef struct memb_size memb_size_s;
struct memb_size {
    unsigned char in;
    unsigned char out;
};

typedef void convert_func(size_t, void const*, custom_big_dtype_t const, custom_big_dtype_t const, void*);

int quantizer_run(memb_size_s const, option, convert_func*);
