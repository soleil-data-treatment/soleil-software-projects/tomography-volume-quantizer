#pragma once

#include "common.h"

#define CONFIG_CHUNK_LEN 1048576 // 2048 * 2048 / 4
#define CONFIG_OUTFILE "toto.vol"
#define CONFIG_PIXEL_VALUE_LOW -10.3
#define CONFIG_PIXEL_VALUE_HIGH 10.3
#define CONFIG_UNQUANTIZE false
