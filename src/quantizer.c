#include "quantizer.h"

int quantizer_run(memb_size_s const item_size, option o, convert_func* convert) {

    debug_print("Going from %u bits to %u bits\n", item_size.in * BYTE_TO_BIT, item_size.out * BYTE_TO_BIT);

    void* data = safe_calloc(o.chunk_len, item_size.in);
    void* processed_data = safe_calloc(o.chunk_len, item_size.out);

    FILE* fin = safe_fopen(o.infile, "rb");

    size_t const infile_size = utils_get_file_size(o.infile);

    size_t const chunk_size = o.chunk_len * item_size.in;
    debug_print("chunk size (bytes) = %zu\n", chunk_size);

    size_t const nb_chunks = utils_get_nb_chunks(infile_size, chunk_size);

    FILE* fout = safe_fopen(o.outfile, "wb");

    size_t const processed_chunk_size = o.chunk_len * item_size.out;
    debug_print("processed chunk size (bytes) = %zu\n", processed_chunk_size);

    size_t i;

    for (i = 0; i < nb_chunks; ++i) {

        int read_size = fread(data, 1, chunk_size, fin);
        debug_print("read_size (bytes) = %d\n", read_size);

        convert(o.chunk_len, data, o.pixel_value_low, o.pixel_value_high, processed_data);

        fwrite(processed_data, 1, processed_chunk_size, fout);
    }

    free(data);
    free(processed_data);

    fclose(fin);
    fclose(fout);

    return EXIT_SUCCESS;
}
