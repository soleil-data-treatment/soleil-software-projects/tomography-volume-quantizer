#include "option.h"

int option_usage() {
    char help_str[PATH_MAX];
    fprintf(stderr, "Usage: ./main.exe INFILE\n");
    sprintf(help_str, " [ -c CHUNK_LENGTH ] -- Chunk length [default: %d]\n", CONFIG_CHUNK_LEN);
    fprintf(stderr, help_str);
    sprintf(help_str, " [ -a PIXEL_VALUE_LOW ] -- Lowest pixel value [default: " BIG_DTYPE_FMT_STR "]\n", CONFIG_PIXEL_VALUE_LOW);
    fprintf(stderr, help_str);
    sprintf(help_str, " [ -b PIXEL_VALUE_HIGH ] -- Highest pixel value [default: " BIG_DTYPE_FMT_STR "]\n", CONFIG_PIXEL_VALUE_HIGH);
    fprintf(stderr, help_str);
    sprintf(help_str, " [ -o OUTFILE ] -- Output file [default: %s]\n", CONFIG_OUTFILE);
    fprintf(stderr, help_str);
    sprintf(help_str, " [ -u ] -- For unquantizing [default: %d]\n", CONFIG_UNQUANTIZE);
    fprintf(stderr, help_str);
    exit(0);
}

option option_get(int argc, char* argv[argc+1]) {
    /* Default option values */
    option o = {
        .chunk_len = CONFIG_CHUNK_LEN,
        .outfile = CONFIG_OUTFILE,
        .unquantize = CONFIG_UNQUANTIZE,
        .pixel_value_low = CONFIG_PIXEL_VALUE_LOW,
        .pixel_value_high = CONFIG_PIXEL_VALUE_HIGH
    };

    int index, opt;
    while ((opt = getopt(argc, argv, "a:b:c:ho:u")) != -1)
        switch (opt) {
            case 'a':
                sscanf(optarg, BIG_DTYPE_FMT_STR, &o.pixel_value_low);
                break;
            case 'b':
                sscanf(optarg, BIG_DTYPE_FMT_STR, &o.pixel_value_high);
                break;
            case 'c':
                sscanf(optarg, "%d", &o.chunk_len);
                break;
            case 'h': 
                option_usage();
                exit(0);
            case 'o': 
                strcpy(o.outfile, optarg);
                break;
            case 'u': 
                o.unquantize = true;
                break;
            case '?':
                if (optopt == 'c')
                    fprintf(stderr, "Option -c requires an argument.\n");
                else if (optopt == 'a' || optopt == 'b')
                    fprintf(stderr, "Option -a and/or -b requires an argument.\n");
                else if (optopt == 'o')
                    fprintf(stderr, "Option -o requires an argument.\n");
                else if (isprint(optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
                exit(EXIT_FAILURE);
            default: 
                abort();
        }

    debug_print("chunk_len = %d\n", o.chunk_len);
    debug_print("pixel_value_low = " BIG_DTYPE_FMT_STR "\n", o.pixel_value_low);
    debug_print("pixel_value_high = " BIG_DTYPE_FMT_STR "\n", o.pixel_value_high);
    debug_print("outfile = %s\n", o.outfile);
    debug_print("unquantize = %d\n", o.unquantize);

    debug_print("# args = %d, optind = %d\n", argc, optind);
    for (index = optind; index < argc; ++index) {
        debug_print("Non-option argument %s\n", argv[index]);
    }

    if ((argc - optind) != 1) {
        option_usage();
    }

    assert(o.chunk_len > 0);
    assert(o.pixel_value_low < o.pixel_value_high);

    char* infile = argv[optind];
    if (!infile || !utils_file_exists(infile))
        exit(EXIT_FAILURE);

    o.infile = infile;
    debug_print("Input file = %s\n", o.infile);

    return o;
}
