#pragma once

#include "common.h"

void chunk_quantize(size_t, void const*, custom_big_dtype_t const, custom_big_dtype_t const, void* out);
void chunk_unquantize(size_t, void const*, custom_big_dtype_t const, custom_big_dtype_t const, void* out);
