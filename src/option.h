#pragma once

#include "common.h"
#include "config.h"
#include "utils.h"

typedef struct option option;

struct option {
    /* Mandatory */
    char* infile;
    /* Optional */
    int chunk_len;
    char outfile[PATH_MAX];
    custom_big_dtype_t pixel_value_low;
    custom_big_dtype_t pixel_value_high;
    bool unquantize;
};

int option_usage();
option option_get(int, char**);

