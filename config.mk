CC := gcc

CFLAGS := -O2 -Wall

TEST_VOL_FILE := example/raw.vol
TEST_VOLQ_FILE := example/quantized.vol
TEST_VOLU_FILE := example/unquantized.vol
TEST_PIXEL_VALUE_LOW = -5.0
TEST_PIXEL_VALUE_HIGH = 5.0

RM := rm -f
RMDIR := rm -r

QUIET = @

