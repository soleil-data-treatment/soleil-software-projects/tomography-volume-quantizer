#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import struct

SLICE_NO = 0
IMAGE_SHAPE = (2048, 2048)

LITTLE_ENDIAN_CODE = '<'
BIG_ENDIAN_CODE = '>'
ENDIAN_CODE_CHOICES = [LITTLE_ENDIAN_CODE, BIG_ENDIAN_CODE]

UINT8_CODE = 'B'
UINT16_CODE = 'H'
FLOAT32_CODE = 'f'
DTYPE_CODE_CHOICES = [UINT8_CODE, UINT16_CODE, FLOAT32_CODE]


def get_chunk_len(shape):
    """Return the chunk length based on the image dimension"""
    return shape[0] * shape[1]


def get_image(infile, image_shape=IMAGE_SHAPE, slice_no=SLICE_NO, dtype_code=FLOAT32_CODE, endianess_code=LITTLE_ENDIAN_CODE):
    """Read chunks of data"""
    dtype = endianess_code + dtype_code
    with open(infile, 'rb') as f:
        chunk_len = get_chunk_len(image_shape)
        chunk_size_bytes = chunk_len * struct.calcsize(dtype)
        offset = slice_no * chunk_size_bytes
        f.seek(0)
        image = np.fromfile(f, dtype=dtype, count=chunk_len, offset=offset).reshape(image_shape)
    return image    


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('vol_file', type=str, help="Input binary float32 little-endian volume file")
    parser.add_argument('-s', '--image-shape', type=int, nargs='+', default=IMAGE_SHAPE, help="Dimension of the the image [e.g., 2048 2048]")
    parser.add_argument('-i', '--slice-no', type=int, default=SLICE_NO, help="Slice number")
    parser.add_argument('-d', '--dtype-code', type=str, choices=DTYPE_CODE_CHOICES , default=FLOAT32_CODE, help="Data-type character code")
    parser.add_argument('-e', '--endianess-code',  type=str, choices=ENDIAN_CODE_CHOICES, default=LITTLE_ENDIAN_CODE, help="Endianess")
    args = parser.parse_args()

    data = get_image(args.vol_file, image_shape=args.image_shape, slice_no=args.slice_no, dtype_code=args.dtype_code, endianess_code=args.endianess_code)
    print("Min pixel value in slice: {}".format(data.min()))
    print("Max pixel value in slice: {}".format(data.max()))

    # Histogram
    bins = 256
    counts, bin_edges = np.histogram(data, bins=bins)
    print(bin_edges)
    print(counts)

    plt.hist(bin_edges[:-1], bin_edges, weights=counts, histtype='stepfilled', log=True)
    plt.show()

    # Plot slice image
    ax = plt.subplot()
    ax.title.set_text(args.vol_file)
    im = ax.imshow(data)

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)

    plt.colorbar(im, cax=cax)
    plt.show()
