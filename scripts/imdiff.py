#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import struct

from read_vol import SLICE_NO
from read_vol import IMAGE_SHAPE
from read_vol import LITTLE_ENDIAN_CODE
from read_vol import ENDIAN_CODE_CHOICES
from read_vol import FLOAT32_CODE
from read_vol import DTYPE_CODE_CHOICES
from read_vol import get_image


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('vol_files', type=str, nargs=2, help="Two input binary float32 little-endian volume files")
    parser.add_argument('-s', '--image-shape', type=int, nargs='+', default=IMAGE_SHAPE, help="Dimension of the the image [e.g., 2048 2048]")
    parser.add_argument('-i', '--slice-no', type=int, default=SLICE_NO, help="Slice number")
    parser.add_argument('-d', '--dtype-code', type=str, choices=DTYPE_CODE_CHOICES , default=FLOAT32_CODE, help="Data-type character code")
    parser.add_argument('-e', '--endianess-code',  type=str, choices=ENDIAN_CODE_CHOICES, default=LITTLE_ENDIAN_CODE, help="Endianess")
    args = parser.parse_args()

    vol_file_1, vol_file_2 = args.vol_files
    im1 = get_image(vol_file_1, image_shape=args.image_shape, slice_no=args.slice_no, dtype_code=args.dtype_code, endianess_code=args.endianess_code)
    im2 = get_image(vol_file_2, image_shape=args.image_shape, slice_no=args.slice_no, dtype_code=args.dtype_code, endianess_code=args.endianess_code)
   
    data = im1 - im2

    # Plot image difference
    ax = plt.subplot()
    ax.title.set_text(args.vol_files)
    im = ax.imshow(data)

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)

    plt.colorbar(im, cax=cax)
    plt.show()
