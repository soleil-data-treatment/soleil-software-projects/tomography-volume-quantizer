#!/usr/bin/env python
# -*- coding: utf-8 -*-

import struct
from read_vol import INT8_CODE

BYTE_TO_BIT = 8


def quantize(data, vlow, vhigh, dtype_code=INT8_CODE):
    """Quantize the data"""
    vrange = vhigh - vlow
    n = struct.calcsize(dtype_code) * BYTE_TO_BIT
    int_size_min = 0
    int_size_max = 2**n - 1
    l = (int_size_max - int_size_min) / vrange
    m = (int_size_min * vhigh - int_size_max * vlow) / vrange
    data[data < vlow] = vlow
    data[data > vhigh] = vhigh
    qdata = np.round(l * data + m)
    return qdata


def unquantize(qdata, vlow, vhigh, dtype_code=INT8_CODE):
    """Unquantize the quantized data"""
    vrange = vhigh - vlow
    n = struct.calcsize(dtype_code) * BYTE_TO_BIT
    int_size_min = 0
    int_size_max = 2**n - 1
    l = (int_size_max - int_size_min)
    m = (int_size_min * vhigh - int_size_max * vlow) / vrange
    udata = (qdata - m) / l
    return udata