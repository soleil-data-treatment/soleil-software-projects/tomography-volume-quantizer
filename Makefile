CONFIG ?= config.mk
include $(CONFIG)

LDLIBS := -lm

SOURCE := main.c
EXEC := $(SOURCE:.c=.exe)

SOURCES := $(wildcard src/*.c)
OBJECTS := $(SOURCES:.c=.o)

.PHONY: all debug check clean distclean

all: $(EXEC)

$(EXEC): $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)

src/%.o: src/%.c
	$(CC) $(CFLAGS) -c $^ -o $@ $(LDFLAGS) $(LDLIBS)

debug: CFLAGS = -O0 -Wall
debug: CFLAGS += -g -DDEBUG
debug: CFLAGS += -pg
debug: $(EXEC)

openmp: CFLAGS += -DFLAG_OPENMP
openmp: CFLAGS += -fopenmp
openmp: $(EXEC)

openmp-debug: CFLAGS = -O0 -Wall
openmp-debug: CFLAGS += -g -DDEBUG
openmp-debug: openmp

check:
	$(QUIET) echo using $(CONFIG)
	$(QUIET) echo quantizing the raw volume
	./$(EXEC) -a $(TEST_PIXEL_VALUE_LOW) -b $(TEST_PIXEL_VALUE_HIGH) -o $(TEST_VOLQ_FILE) $(TEST_VOL_FILE)
	$(QUIET) echo unquantizing the quantized volume
	./$(EXEC) -u -a $(TEST_PIXEL_VALUE_LOW) -b $(TEST_PIXEL_VALUE_HIGH) -o $(TEST_VOLU_FILE) $(TEST_VOLQ_FILE)

clean:
	$(RM) $(OBJECTS)
	$(RM) $(TEST_VOLQ_FILE) $(TEST_VOLU_FILE)
	$(RM) gmon.out

distclean: clean
	$(RM) *.exe
