#/bin/bash

usage() {
    echo "Usage: $0 VOL_FILE VOL_INFO_FILE"
}

if [  $# -lt 2 ]; then
    usage
    exit 1
fi

INFILE_VOL=$1
INFILE_INFO=$2


NX=$(cat ${INFILE_INFO} | grep "NUM_X" | cut -d "=" -f 2 | tr -d ' ')
NY=$(cat ${INFILE_INFO} | grep "NUM_Y" | cut -d "=" -f 2 | tr -d ' ')

CHUNK_LEN=$(echo "${NX} * ${NY}" | bc)

VALUE_LOW=$(cat ${INFILE_INFO} | grep "S1" | cut -d "=" -f 2 | tr -d ' ')
VALUE_HIGH=$(cat ${INFILE_INFO} | grep "S2" | cut -d "=" -f 2 | tr -d ' ')

OUTFILE_VOL="${INFILE_VOL%.vol}_16b.vol"

sbatch -o tvq.log --job-name=tvq --export==CHUNK_LEN=${CHUNK_LEN},VALUE_LOW=${VALUE_LOW},VALUE_HIGH=${VALUE_HIGH},INFILE=${INFILE_VOL},OUTFILE=${OUTFILE_VOL} tvq_job.sh
