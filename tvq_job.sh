#!/bin/bash

#SBATCH --output=a.log
#SBATCH --partition=debug

#SBATCH --time=10:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=100

./main16.exe -c ${CHUNK_LEN} -a ${VALUE_LOW} -b ${VALUE_HIGH} -o ${OUTFILE} ${INFILE}

